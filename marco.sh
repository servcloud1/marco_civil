#/bin/bash
# Author: ServCloud
# Date: 21/04/2016 - Version 1.0 - Original Version
#
# Author: ServCloud
#
# Added Removal content in 200 days on /backup/Marco_Civil_Backup and if runned 2 times, backup of logs will be done
# Update: 29/06/2019 - Version 1.1
#
# Started update, pastebins have been gone, change for gitlab and optimize/remove that dont need use wget
# Updated: 13/04/2020 - Version 1.2 (Bug Fixes)
#
#
# Last Updated: 12-05-20 (Quarentine Time) 100% Updated - Version 2.0
# Code Optimization have been done.
# Added Removal of duplicate content on /etc/cpanelsync.exclude

# Main
clear
echo;
echo "Marco Civil is starting... Please wait"
echo;

# Backup de cada um dos arquivos atuais em /etc/logrotate.d/ (apache, cpanel, exim, syslog, ssh)
mv /backup/Marco_Civil_Backup/ /backup/Marco_Civil_Backup`date +"%m-%d-%y"`/ && echo "Existia um diretório em /backup/Marco_Civil_Backup e foi movido" || echo "Erro ao mover o diretorio /backup/Marco_Civil_Backup/"
wget https://gitlab.com/servcloud1/marco_civil/-/raw/main/logstobesaved -O giropops >/dev/null 2>&1
tr -d '\15\32' < giropops > giropops.txt
for i in `cat giropops.txt`; do mv -f $i $i.bkp > /dev/null 2>&1; done
for i in `cat giropops.txt`; do echo "$i" >> /etc/cpanelsync.exclude; done

#Remove tudo que for repetido no arquivo de imutáveis do cPanel
var1=$(cat /etc/cpanelsync.exclude)
echo $var1 | xargs -n 1 | sort | uniq > /etc/cpanelsync.exclude
rm -f giropops.txt giropops








# Baixa os novos arquivos e adiciona no diretório correto
wget https://gitlab.com/servcloud1/marco_civil/-/raw/main/apache -O /etc/logrotate.d/apache >/dev/null 2>&1
wget https://gitlab.com/servcloud1/marco_civil/-/raw/main/cpanel -O /etc/logrotate.d/cpanel >/dev/null 2>&1
wget https://gitlab.com/servcloud1/marco_civil/-/raw/main/exim -O /etc/logrotate.d/exim >/dev/null 2>&1
wget https://gitlab.com/servcloud1/marco_civil/-/raw/main/syslog -O /etc/logrotate.d/syslog >/dev/null 2>&1
wget https://gitlab.com/servcloud1/marco_civil/-/raw/main/ssh -O /etc/logrotate.d/ssh >/dev/null 2>&1


# Cria os diretórios de armazenamento
dirs1="/backup/Marco_Civil_Backup/apache 
/backup/Marco_Civil_Backup/cpanel 
/backup/Marco_Civil_Backup/exim_mainlog 
/backup/Marco_Civil_Backup/maillog 
/backup/Marco_Civil_Backup/messages 
/backup/Marco_Civil_Backup/ssh"
mkdir -p $dirs1

# Baixa o script de sincronização p/ o cron
wget https://gitlab.com/servcloud1/marco_civil/-/raw/main/sync -O marco >/dev/null 2>&1
tr -d '\15\32' < marco > /root/marco.sh && chmod +x /root/marco.sh
rm -f marco

# Adiciona o cron

#remove caso exista, qualquer ou quantas linhas
sed -i '/marco.sh/d' /var/spool/cron/root

#Adiciona linha
echo "0 22 *  * 1 /root/marco.sh > /dev/null 2>&1" >> /var/spool/cron/root

# Mostra o os arquivos finais, diretórios e o cron.
clear
echo;
echo "Finalizado, segue arquivos, diretórios e o cron adicionado: "
echo;
wget https://gitlab.com/servcloud1/marco_civil/-/raw/main/logstobesaved -O ggtt >/dev/null 2>&1
tr -d '\15\32' < ggtt > ggtt.txt
for i in `cat ggtt.txt`; do ls -l $i; done && rm -f ggtt ggtt.txt
echo;
echo "Diretório /backup/Marco_Civil_Backup/"; echo
ls -l /backup/Marco_Civil_Backup/ | egrep -v "total" | tail -n6
echo;
grep marco /var/spool/cron/root
echo;
mkdir /backup/Marco_Civil_Backup/oldconfig
mv /etc/logrotate.d/*.bkp /backup/Marco_Civil_Backup/oldconfig
echo "Arquivos imutáveis"
echo;
cat /etc/cpanelsync.exclude;
echo;
echo -e "Sincronizando a primeira vez, por favor aguarde"
bash /root/marco.sh >/dev/null 2>&1 
echo -e "Finalizado"
